from network import WLAN
import machine
import pycom
import time
import config


def connect_wifi():
    red = 0x110000
    green = 0x001100
    wlan = WLAN(mode=WLAN.STA)

    wlan.connect(
        ssid=config.WIFI_SSID,
        auth=(WLAN.WPA2, config.WIFI_PASSWORD)
        )

    pycom.heartbeat(False)
    pycom.rgbled(red)

    while not wlan.isconnected():
        machine.idle()
        time.sleep(1)

    pycom.rgbled(green)
    print('Wifi is connected succesfully')
    print(wlan.ifconfig())
