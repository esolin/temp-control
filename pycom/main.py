import machine
import ubinascii
from mqtt import MQTTClient_lib as MQTTClient
from machine import Pin
from dht import DHT
import config
import time
import _thread


thread_nr = 0
client_id = ubinascii.hexlify(machine.unique_id()).decode('utf-8')

relay = Pin('P12', mode=Pin.OUT)
sensor = DHT(Pin('P11', mode=Pin.OPEN_DRAIN), 0)
switch = Pin('P10', mode=Pin.IN, pull=Pin.PULL_DOWN)
led_red = Pin('P9', mode=Pin.OUT)
led_green = Pin('P8', mode=Pin.OUT)

client = MQTTClient(
    client_id,
    config.BROKER_URL,
    user=config.USER,
    password=config.PASSWORD,
    port=config.PORT
    )

def set_state():
    if (relay() == 1):
        led_green(1)
        led_red(0)
        
        client.publish(
            topic=config.TOPIC_PUB_IS_ON,
            msg='true'
            )
    else:
        led_red(1)
        led_green(0)
        
        client.publish(
            topic=config.TOPIC_PUB_IS_ON,
            msg='false'
            )


def switch_th(id):
    while True:
        try:
            while (switch() == 0):
                machine.idle()

            relay.toggle()
            set_state()
        except Exception:
            print('Something bad just happened with the switch')            
        time.sleep(1)


def sensor_th(id):
    time.sleep(2)

    while True:
        try:
            result = sensor.read()
            while not result.is_valid():
                time.sleep(.5)
                result = sensor.read()
            
            client.publish(
                topic=config.TOPIC_PUB_TEMP,
                msg=str(result.temperature)
                )

            client.publish(
                topic=config.TOPIC_PUB_HUMIDITY,
                msg=str(result.humidity)
                )
        except Exception:
            print('Something bad just happened with the sensor')
        time.sleep(5)


def sub_cb(topic, power_on):
    power = power_on.decode('utf-8')
    if (power == 'true'):
        relay(1)
    else:
        relay(0)


client.set_callback(sub_cb)
client.connect()
client.subscribe(topic=config.TOPIC_SUB_POWER)

_thread.start_new_thread(switch_th, (thread_nr,))
thread_nr += 1

_thread.start_new_thread(sensor_th, (thread_nr,))
thread_nr += 1


while True:
    try:
        client.wait_msg()
        set_state()
    except Exception:
        print('Something bad just happened with mqtt')    
    time.sleep(1)

